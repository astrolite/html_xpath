(ns html_xpath.core.test
  (:require [html_xpath.core :refer [parse to-xml]]
            [clojure.test    :refer [deftest is]]))

(deftest test1 []
  (is (= (:value
          (first
           (first
            (parse "<p><table> <tr><td><tr><td>23<td>asd</table>"
                   ["//tr[2]/td[2]/text()"]))))
         "asd")))

(deftest test2 []
  (is (= (:value
          (first
           (first
            (parse "<p><b>ra</b>yes<br>"
                   ["//b/following-sibling::text()"]))))
         "yes")))

(deftest test3 []
  (let [html "<p><div><legend><h2>название</h2></legend><div id=qq>aa</div><div>ii</div></div>"
        [[{[{val :value}] :children :as node}]]
        (parse html
               ["//h2[text()='название']/following-sibling::div[1]"])]
    (is (= val "aa"))
    (is (= (to-xml node) "<div id=\"qq\">aa</div>"))))

