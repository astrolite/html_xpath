(defproject html_xpath "0.0.1"
  :source-paths ["src"]
  :resource-paths ["resources"]
  :repl-options {:timeout 300000}
  :main html_xpath.core
  :plugins [[lein-cljfmt "0.6.0"]]
  :profiles {:uberjar {:aot :all}}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.jdom/jdom2 "2.0.6"]
                 [net.sourceforge.htmlcleaner/htmlcleaner "2.22"]
                 [org.ccil.cowan.tagsoup/tagsoup "1.2.1"]
                 ])
