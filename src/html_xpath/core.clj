(ns html_xpath.core
  (:gen-class))

(declare xerces-to-clj get-attrs get-children)
(defn parse [html xpaths]
  ;; https://stackoverflow.com/questions/9022140/using-xpath-contains-against-html-in-java
  ;; http://htmlcleaner.sourceforge.net/doc/index.html
  ;; https://stackoverflow.com/questions/3361263/library-to-query-html-with-xpath-in-java
  (let [theXpath (-> (javax.xml.xpath.XPathFactory/newInstance) .newXPath)
        htmlNode
        (if (get #{com.sun.org.apache.xerces.internal.dom.ElementNSImpl
                   com.sun.org.apache.xerces.internal.dom.DocumentImpl} (class html))
          html
          #_
          (.createDOM (org.htmlcleaner.DomSerializer. (org.htmlcleaner.CleanerProperties.))
                      (.clean (org.htmlcleaner.HtmlCleaner.)
                              html))
          ;;#_
          (let [input (java.io.ByteArrayInputStream. (.getBytes html "UTF-8"))
                reader (org.ccil.cowan.tagsoup.Parser.)
                _ (.setFeature reader org.ccil.cowan.tagsoup.Parser/namespacesFeature false)
                transformer (-> (javax.xml.transform.TransformerFactory/newInstance) .newTransformer)
                result (javax.xml.transform.dom.DOMResult.)
                _ (.transform transformer (javax.xml.transform.sax.SAXSource. reader (org.xml.sax.InputSource. input)) result)]
            (.getNode result)))]
    (for [xpath xpaths]
      (let [nodes (.evaluate theXpath xpath htmlNode javax.xml.xpath.XPathConstants/NODESET)]
        (map (fn [n]
               (xerces-to-clj (.item nodes n)))
             (range (.getLength nodes)))))))

(defn xerces-to-clj [node]
  {:name (.getNodeName node)
   :attrs (get-attrs (.getAttributes node))
   :value (cond
            (= com.sun.org.apache.xerces.internal.dom.TextImpl (class node)) (.getWholeText node)
            (= com.sun.org.apache.xerces.internal.dom.AttrNSImpl (class node)) (.getValue node)
            (= com.sun.org.apache.xerces.internal.dom.CommentImpl (class node)) (.getNodeValue node))
   :children (get-children (.getChildNodes node))
   :node node})

(defn get-attrs [node]
  (when node
    (into {}
          (map (fn [n]
                 (let [subnode (.item node n)]
                   [(keyword (.getName subnode))
                    (.getValue subnode)]))
               (range (.getLength node))))))

(defn get-children [nodes]
  (when nodes
    (map #(xerces-to-clj (.item nodes %))
         (range (.getLength nodes)))))

(defn to-xml [dom]
  (if (= (:name dom) "#text")
    (:value dom)
    (let [children (vec (:children dom))]
      (str "<"
           (:name dom)
           (clojure.string/join
            ""
            (map (fn [[k v]]
                   (str " " (name k)
                        "=\""
                        (clojure.string/replace v #"\"" "&quot;")
                        "\""))
                 (:attrs dom)))
           (if (count children)
             (str ">"
                  (clojure.string/join "" (map to-xml children))
                  "</" (:name dom) ">")
             "/>")))))

